# SKOS

이 사이트는 SKOS에 대한 literature들에 대한 모음이다.

- [SKOS Primer](https://suredata.gitlab.io/dcat-ap-knpp/w3c-related/skos-primer/)
- [SKOS: A Guide for Information Professionals](https://suredata.gitlab.io/dcat-ap-knpp/w3c-related/skos/)

### References
- [Taxonomy management with SKOS](https://www.bobdc.com/blog/skosibm/)
