# SKOS: 정보 전문가를 위한 안내서 <sup>[1](#footnote_1)</sup>

## A Guide to Representing Structured Controlled Vocabularies in the Simple Knowledge Organization System

### 요약
Simple Knowledge Organization System(SKOS)와 관련 웹 기술은 기존의 통제 어휘(controlled vocaburary)를 웹에서 사용할 수 있도록 하여 어휘 작성자가 디지털 어휘를 웹에 게시할 수 있도록 하는 것을 목표로 한다.

이 가이드는 카탈로그 개발자(cataloger), 사서 및 기타 정보 전문가가 웹 환경에서 사용할 수 있도록 제어 어휘의 표현을 위해 설계된 W3C 표준인 SKOS를 이해하여 사용할 수 있도록 도와주기 위한 것이다.

이 가이드에는 분류 기술의 간략한 역사, SKOS의 역사, XML 및 RDF와 같은 관련 기술 표준의 맥락에서 SKOS를 살펴보는 내용이 포함되어 있다. SKOS 어휘의 요소와 구문에 대한 논의에 이어, SKOS 어휘를 구성하는 모범 사례의 지침으로 사용되는 다양한 무결성 조건을 설명한다. 그런 다음 과거 문헌을 검토하여 SKOS 어휘의 변환, 유효성 검사, 개선 및 자동 생성을 강조한다. 마지막 섹션에서는 SKOS 개발의 향후 방향에 대해 논의한다.

## 소개
역사적으로 분류 시스템(classification system)은 도서관 커뮤니티에서 널리 사용되어 왔다. KOS(Knowledge Organization System), 더 구체적으로는 통제와 구조화된 어휘는 분류 시스템 분야에서 성장하고 있는 영역이다. 웹 컨텍스트 내에서 웹 표준 XML과 RDF를 사용하여 구조화된 방식으로 통제 어휘를 표현하는 형식이 제안되었다.

웹에 게시된 통제 어휘와 웹을 위해 특별히 구조화된 방식으로 게시된 어휘를 구분하는 것이 중요하다. 간단한 주제 표제어 목록, 시소러스와 책 뒤의 색인같은 자연어 어휘는 인간에게는 매우 유용하지만 기계가 이를 통해 도출할 수 있는 의미는 매우 제한적이다. 링크된 오픈 데이터(linked open data)와 링크된 오픈 어휘(linked open vocaburary)는 모두 시맨틱 웹 기술로, 인간과 기계가 모두 의미를 인식할 수 있도록 통제 어휘를 웹에 게시할 수 있도록 한다(Kaltenböck and Bauer 2012). 링크된 개방형 어휘를 생성하면 더 의미 있는 접근과 발견 지점을 늘려 정보 검색의 효율성을 높일 수 있다.

통제 어휘는 나중에 쉽게 검색할 수 있도록 일부 콘텐츠 또는 지식을 정리할 수 있도록 한다. 이러한 어휘는 포함된 콘텐츠의 공인된 식별을 사용한다는 점에서 "통제된" 어휘이다. 이러한 개념 그룹은 신중하게 선택되고 설명되어 포함된 정보를 가능한 가장 지능적인 방법으로 검색할 수 있다. 예를 들어 [그림 1](#fig-1)에서 볼 수 있는 채소 어휘라는 아주 작은 용어 모음을 생각해 보자. 이 채소에 관한 용어 그룹은 알파벳 순서을 나열하였다. 한눈에 보기에 이 목록은 식품 이름(영어)과 종 이름(라틴어)으로 구성되어 있음을 알 수 있다. 그러나 동일한 정보를 다른 방식으로 표시하여 사람들이 서로 다른 용어 간의 관계를 훨씬 쉽게 이해할 수 있도록 할 수 있다.

![fig-1](./images/fig_1.png)<br>
<a name="fig-1">그림 1</a>

[그림 2](#fig-2)의 시각화를 보면 어휘가 채소의 종류에 관한 것임을 쉽게 알 수 있으며, `Vegitables`가 어휘에서 "top" 용어임을 알 수 있다. `Bean`, `Root`와 `Gourd`이라는 용어는 `Vegitables` 바로 아래에 있다. 이 용어들은 채소의 종류를 설명한다. 이 두 번째 수준의 용어 아래에는 `Potato`와 `Pumpkin`같은 실제 채소 용어 그룹이 있다. 용어 간의 관계는 계층적 관계, 즉 서로에 대한 범위가 더 넓거나 좁다. 통제 어휘에서 계층적 관계의 두 가지 유형은 *광의 용어(BT: broader term)*와 *협의 용어(NT:narrower term)*이다. 예를 들어 `Root`는 `Vegitables`와 관련하여 광의 용어이지만 `Parsnip`과 관련하여 협의 용어이다.

![fig-2](./images/fig_2.png)<br>
<a name="fig-2">그림 2</a>

어휘를 시각화하는 또 다른 방법은 계층형 구조이다. [그림 3](#fig-3)은 용어와 용어가 연관된 용어의 계층 구조를 보인다. 이 그람에는 `UF`라는 레이블이 붙은 `Veggie`라는 용어도 포함되어 있다. *use for(UF)* 또는 *use(USE)*같은 동등성 관계를 통해 어휘는 동의어와 동의어에 가까운 어휘를 연결할 수 있다. 이 특정 어휘에서 `Vegetable`은 승인되거나 선호되는 용어이고, `Veggie`는 같은 개념에 대해 승인되지 않거나 선호되지 않는 용어이다.

어휘에 반영할 수 있는 마지막 관계 타입은 연관 관계이다. 이 타입의 관계를 통해 어휘는 *관련 용어(RT: related term)* 또는 계층 관계나 등가 관계가 없는 용어 간에 연결을 만들 수 있다. 예를 들어, `Fruit`은 `Vegetable`와 연관된 용어일 수 있다.

![fig-3](./images/fig_3.png)<br>
<a name="fig-3">그림 3</a>

## SKOS 정의
SKOS는 시소러스, 분류 체계, 주제 표제 시스템 및 분류 체계같은 KOS을 위한 공통 데이터 모델이다. SKOS를 사용하면 KOS나 통제 어휘를 기계 판독 가능한 데이터로 표현할 수 있다. SKOS로 표현된 데이터는 컴퓨터 어플리케이션 간에 교환할 수 있고 웹에 기계 판독 가능한 형식으로 게시할 수 있다.

SKOS-Core 1.0 가이드는 2001년 W3C [Semantic Web Deployment Working Group(SWDWG)](http://www.w3.org/2006/07/SWD/)에서 SKOS를 W3C 표준 분류 체계로 개발하기 위해 처음 소개되었다. W3C SWDWG는 현재 웹에서 무료로 이용할 수 있는 SKOS에 관한 여러 문서를 관리하고 있다. 첫째, 현재 최종 W3C 권고 또는 표준 단계에 있는 [SKOS Reference](http://www.w3.org/TR/skos-reference/) 문서에서 SKOS를 정의한다. 둘째, [SKOS Primer](https://sdldocs.gitlab.io/w3c-related/skos-primer/) 문서는 시스템 사용자를 위한 가이드를 제공한다. 셋째, [SKOS Use Cases and Requirements](http://www.w3.org/TR/skos-ucr/) 문서는 대표적인 사용 사례 목록과 이러한 사용 사례에서 파생된 일련의 요구 사항을 제시한다. 또한 SWDWG는 공개 메일링 리스트와 [wiki](http://www.w3.org/2006/07/SWD/wiki/)를 운영하여 대중이 SKOS 개발에 기여할 수 있도록 하고 있다.

SKOS Reference 문서에 따르면 이 시스템의 목표는 다음과 같다.

1. "지식 조직 시스템(kowledge organization system)"의 설계와 적용에 관여하는 도서관과 정보과학 내의 다양한 실무 커뮤니티를 연결하는 다리를 제공하는 것".
2. "기존 지식 조직 모델을 시맨틱 웹 기술 컨텍스트로 이전하고 기존 지식 조직 시스템을 RDF로 포팅하기 위한 저비용 마이그레이션 경로를 제공함으로써 이러한 커뮤니티와 시맨틱 웹 간의 가교를 제공하는 것".

SKOS는 공식적인 논리와 구조를 위해 기존의 여러 시맨틱 웹 표준을 기반으로 구축된 데이터 공유 표준이다. 이러한 기술은 계산이 가능하고 웹에 이미 존재하는 정보를 보완하고 구조를 부여하는 의미를 표현하는 방법을 제공한다. SKOS는 RDF를 기반으로 구축되었기 때문에 SKOS 데이터를 RDF 트리플로 표현할 수 있다. RDF와 기타 관련 웹 기술은 [여기](https://sdldocs.gitlab.io/w3c-related/contents/)에서 볼 수 있다.

## SKOS의 요소

### SKOS 어휘
SKOS의 어휘에는 지식 조직 시스템을 표현하기 위해 함께 작동하는 다양한 요소가 포함되어 있다. 이러한 요소에는 개념(concepts), 레이블(labels), 관계(relationships), 매핑 속성(mapping properties), 컬렉션(collections) 및 노트(notes) 등이 있다.

#### 개념(Concepts)
**`skos:Concept`**([`owl:Class`](http://www.w3.org/TR/owl-ref/#Class)의 인스턴스). SKOS 요소에 대한 웹 온톨로지 언어(OWL: Web Ontology Language)와 리소스 설명 프레임워크 스키마(RDFS: Resource Description Framework Schema)의 구문론적 등가물은 참조용으로 제공된다.

SKOS 개념은 아이디어, 객체, 이벤트 등 모든 사고의 단위이다. 이러한 개념은 많은 지식 조직 시스템의 빌딩 블록이다. 개념은 머릿속에 존재하는 추상적인 아이디어이기 때문에 이를 설명하는 데 사용되는 용어와는 독립적이다. 당근을 예로 들어 보자. 토끼가 즐겨 먹는 주황색 채소를 설명하기 위해 사용하는 영어 단어 “carrot”은 사실 당근이라는 개념과는 별개이다. 개념과 그 설명자(또는 레이블)를 별개의 개체로 보는 개념은 기계는 식별자를 통해 개념을 식별하고 인간은 레이블을 통해 개념을 식별할 수 있도록 하기 때문에 SKOS 모델에서 매우 중요하다. 어휘 빌더는 SKOS 개념 요소를 사용하여 개념과 그 레이블을 설명하고 구별할 수 있다. SKOS 개념은 두 단계로 생성할 수 있다.

1. 개념을 고유하게 식별하기 위해 URI(유니폼 리소스 식별자)를 만들거나 재사용한다.
2. `rdf:type` 속성을 사용하여 RDF에서 이 URI로 식별되는 리소스가 `skos:Concept` 타입임을 선언한다.

예:

```rdf
<http://www.veggievocab.com/vegetable/root/carrot> rdf:type skos:Concept
```

#### 레이블(Labels)
SKOS에서 레이블은 개념의 설명자 역할을 하는 요소이다. 세 가지 SKOS 레이블 요소는 RDFS 요소 `rdfs:label`의 하위 속성(sub-properties)이다. 이 세 요소의 목적은 `skos:Concept`를 RDF 일반 리터럴 또는 문자 문자열에 연결하는 것이다.

**`skos:prefLabel`**([`owl:AnnotationProperty`](http://www.w3.org/TR/owl-ref/#Annotations)의 인스턴스와 [`rdfs:label`](http://www.w3.org/TR/rdf-schema/#ch_label)의 하위 속성)

선호 레이블(Preferred Label)은 개념에 승인된 이름을 할당할 수 있게 해주는 SKOS 요소이다. 다음 두 예는 `Vegetable`이라는 개념에 대한 선호 레이블이 영어로는 "vegetable", 프랑스어로는 "légume"이라는 단어임을 보인다. 예

```rdf
ex:vegetable rdf:type skos:Concept;
skos:prefLabel "Vegetable".
```

예

```rdf
ex:vegetable rdf:type skos:Concept;
skos:prefLabel "Vegetable"@en;
skos:prefLabel "Légume"@fr.
```

정보 검색과 정보 조직화 목적을 위해 한 지식 조직 시스템의 두 개념에 주어진 언어 태그에 대해 동일한 선호 레이블을 지정할 수 없다.

**`skos:altLabel`**([`owl:AnnotationProperty`](http://www.w3.org/TR/owl-ref/#Annotations)의 인스턴스와 [`rdfs:label`](http://www.w3.org/TR/rdf-schema/#ch_label)의 하위 속성)

대체 레이블을 사용하면 개념에 승인되지 않은 이름을 지정할 수 있다. 이 레이블을 사용하면 개념에 대해 동일한 언어의 설명자를 여러 개 저장할 수 있다. 다음 예에서는 `fava_bean`이라는 개념에 대한 기본 레이블이 "fava bean"이고 대체 레이블이 "broad bean"임을 보인다. 예

```rdf
ex:fava_bean rdf:type skos:Concept;
skos:prefLabel "Fava bean"@en;
skos:altLabel "Broad bean"@en.
```

**`skos:hiddenLabel`**([`owl:AnnotationProperty`](http://www.w3.org/TR/owl-ref/#Annotations)의 인스턴스와 [`rdfs:label`](http://www.w3.org/TR/rdf-schema/#ch_label)의 하위 속성)

숨겨진 레이블은 지식 조직 시스템 설계자가 텍스트 기반 색인 및 검색 작업을 수행하는 애플리케이션에서 액세스할 수 있지만 그렇지 않은 경우 표시되지 않기를 원하는 리소스에 대한 레이블이다. 다음 예에서는 `Potato`라는 개념에 대해 선호 레이블은 "potato"라는 용어이고, 이 개념에 대한 두 개의 숨겨진 레이블은 "`tater`"와 "`spud`"이다. 숨겨진 레이블은 해당 개념을 지칭하는 데는 사용할 수 있지만 공식 문서에는 적합하지 않은 별명이나 구어체에 사용할 수 있다. 예

```rdf
ex:potato rdf:type skos:Concept;
skos:prefLabel "Potato"@en;
skos:hiddenLabel "Tater"@en.
skos:hiddenLabel "Spud"@en.
```

#### 관계(relationships)
**`skos:broader`**와 **`skos:narrower`** ([`owl:ObjectProperty`](http://www.w3.org/TR/owl-ref/#Property)의 인스탄스)

이 두 SKOS 레이블은 개념 간의 계층적 관계, 즉 한 개념이 다른 개념보다 의미가 더 넓거나 좁다는 것을 나타낸다. 예

```rdf
ex:root rdf:type skos:Concept;
skos:prefLabel "Root Vegetable"@en;
skos:narrower ex:sweet_potato.
```

예

```rdf
ex:sweet_potato rdf:type skos:Concept;
skos:prefLabel "Sweet Potato"@en;
skos:broader ex:root.
```

**`skos:related`**([`owl:ObjectProperty`](http://www.w3.org/TR/owl-ref/#Property)의 인스탄스)

이 SKOS 레이블을 사용하면 디자이너가 두 개념 간의 연관 관계를 선언할 수 있다.

```rdf
ex:vegetable rdf:type skos:Concept;
skos:prefLabel "Vegetable"@en;
skos:related ex:fruit.

ex:fruit rdf:type skos:Concept;
skos:prefLabel "Fruit"@en.
```

SKOS 데이터 모델에서 `skos:related`는 전이 속성으로 정의되지 않으며, `skos:broader`의 전이 클로저(closure)는 `skos:related`와 분리되어 있어야 한다. 개념 `Vegetable`과 `Fruit`이 `skos:related`를 통해 연관된 경우, `Vegetable`에서 `Fruit`로 이어지는 `skos:broader` 관계의 사슬이 없어야 한다. 즉, 개념 간에 계층적 관계와 연관 관계의 인스턴스가 동시에 존재해서는 아니 된다.

#### 시멘틱 관계(Semantic Relationships)
**`skos:semanticRelation`** ([`owl:ObjectProperty`](http://www.w3.org/TR/owl-ref/#Property)의 인스탄스)

SKOS 시맨틱 관계는 SKOS 개념 간의 연결이다. 이러한 타입의 관계는 두 개념 간의 링크가 연결된 개념의 의미가 내재되어 있을 때 발생한다. 각 SKOS 레이블인 `skos:broader`, `skos:narrower`, `skos:broaderTransitive`, `skos:narrowerTransitive` 및 `skos:related`는 `skos:semanticRelation`의 하위 속성이다.

**`skos:broaderTransitive`**와 **`skos:narrowerTransitive`**([`owl:TransitiveProperty`](http://www.w3.org/TR/owl-ref/#TransitiveProperty-def)의 인스턴스)

`skos:broader`와 `skos:narrower`와 마찬가지로 이 두 SKOS 레이블은 개념 간의 계층적 관계, 즉 한 개념이 다른 개념보다 의미가 더 넓거나 좁다고 선언할 수 있다. 이 두 레이블의 전이적 특성은 다음과 같은 문장이 가능하다는 것을 의미한다. "*vegitable*이 *groud*보다 넓고 *groud*이 *pumpkin*보다 넓다면, *vegetable*은 *pumpkin*보다 넓은 것으로 간주된다"와 같은 문장이 SKOS 데이터 모델에서 표현될 수 있다.

#### 매핑 속성(Mapping Properties)
**`skos:mappingRelation`**([`owl:ObjectProperty`](http://www.w3.org/TR/owl-ref/#Property)의 인스턴스)

SKOS 매핑 레이블은 다른 개념 체계(예: 의회도서관 주제 표제(LCSH:Library of Congress Subject Headings), 의학 주제 표제(MeSH: Medical Subject Headings), 그래픽 자료용 시소러스(TGM: Thesaurus for Graphic Materials))에 존재하는 SKOS 개념 간의 매핑(또는 정렬) 연결을 명시하는 데 사용된다.

**`skos:closeMatch`**([`owl:ObjectProperty`](http://www.w3.org/TR/owl-ref/#Property) 및 [`owl:SymmetricProperty`](http://www.w3.org/TR/owl-ref/#SymmetricProperty-def)의 인스턴스)

**`skos:exactMatch`** ([`owl:ObjectProperty`](http://www.w3.org/TR/owl-ref/#Property) 및 [`owl:TransitiveProperty`](http://www.w3.org/TR/owl-ref/#TransitiveProperty-def)의 인스턴스)

**`skos:exactMatch`**는 **`skos:closeMatch`**의 하위 속성이다.

**`skos:broadMatch`** 및 **`skos:narrowmatch`**([`owl:ObjectProperty`](http://www.w3.org/TR/owl-ref/#Property)의 인스턴스)

**`skos:broadMatch`**와 **`skos:narrowMatch`**는 개념 간의 계층적 링크를 지정하는 데 사용된다. 이 두 속성은 서로 반비례 관계에 있다. **`skos:broadMatch`**는 **`skos:broader`**의 하위 속성이고 **`skos:narrowMatch`**는 **`skos:narrower`**의 하위 속성이다.

**`skos:relatedMatch`**([`owl:ObjectProperty`](http://www.w3.org/TR/owl-ref/#Property) 및 [`owl:SymmetricProperty`](http://www.w3.org/TR/owl-ref/#SymmetricProperty-def)의 인스턴스)

#### 개념 컬렉션(Collections of Ceoncepts)
**`skos:Collection`**([`owl:Class`](http://www.w3.org/TR/owl-ref/#Class)의 인스턴스)

SKOS 개념 컬렉션 레이블은 레이블이 지정되거나 정렬된 SKOS 개념 그룹을 설명하는 데 사용된다. 예를 들어 veggie 어휘는 공통점이 있는 개념의 그룹이므로 SKOS 개념의 컬렉션으로 간주할 수 있다.

예

```rdf
<Veggie_Vocab> rdf:type skos:Collection;
 skos:prefLabel “Salad ingredients”@en;
 skos:member ex:lettuce;
 skos:member ex:tomato;
 skos:member ex:onion;
 skos:member ex:cucumber;
 skos:member ex:carrot.
```

**`skos:OrderedCollection`** ([`owl:Class`](http://www.w3.org/TR/owl-ref/#Class)의 인스턴스)

SKOS 개념의 정렬 컬렉션은 특정 타입의 순서, 즉 연대기 또는 알파벳순으로 정렬된 항목 리스트를 캡처하는 데 사용된다.

예

```rdf
<Veggie_Vocab> rdf:type skos:OrderedCollection;
 skos:prefLabel “Vegetables by size”@en;
 skos:memberList :b1.
 _:b1 rdf:first ex:pumpkin;
 rdf:rest _:b2.
 _:b2 rdf:first ex:potato;
 rdf:rest _:b3.
 _:b3 rdf:first ex:limaBean;
 rdf:rest rdf:nil.
```

**`skos:member`**([`owl:ObjectProperty`](http://www.w3.org/TR/owl-ref/#Property)의 인스턴스)

SKOS 개념 멤버는 컬렉션의 여러 멤버를 정의하는 데 사용된다.

예

```rdf
<Veggie_Vocab> rdf:type skos:Collection;
skos:member <bean> , <gourd> , <root> .
```

**`skos:memberList`**([`owl:ObjectProperty`](http://www.w3.org/TR/owl-ref/#Property) 및 [`owl:FunctionalProperty`](http://www.w3.org/TR/owl-ref/#FunctionalProperty-def)의 인스턴스)

SKOS 개념 멤버는 컬렉션의 여러 멤버를 리스트 형식으로 정의하는 데 사용된다.

예

```rdf
<Veggie_Vocab> rdf:type skos:OrderedCollection;
skos:memberList ( <bean> , <gourd> , <root> ) .
```

#### 노트(Notes)
**`skos:Notes`**

이 SKOS 레이블은 일반 문서화를 목적으로 만들었다. 여러 개의 노트를 개별적으로 적절하게 캡처하고 개념과 관련된 추가 정보를 간편하게 검색할 수 있도록 `skos:note`와 다른 전문 분야 사이에는 계층적 링크가 있다.

**`skos:scopeNote`**([`owl:AnnotationProperty`](http://www.w3.org/TR/owl-ref/#Annotations)의 인스턴스)

이 레이블은 개념의 의도된 의미에 대한 몇 가지 정보를 제공한다. 일반적으로 색인 작업에서 개념의 사용이 어떻게 제한되는지를 나타내는 표시로 사용된다.

예

```rdf
ex:root skos:scopeNote
"Used for plant roots used as vegetables"@en.
```

**`skos:definition`** ([`owl:AnnotationProperty`](http://www.w3.org/TR/owl-ref/#Annotations)의 인스턴스)

이 레이블은 개념의 의도된 의미에 대한 완전한 설명을 제공한다.

예

```rdf
ex:parsnip skos:scopeNote
"The parsnip (Pastinaca sativa) is a root vegetable related to the carrot. Parsnips resemble
carrots, but are paler in color than most carrots, and have a sweeter taste, especially when cooked."@en.
```

**`skos:example`** ([`owl:AnnotationProperty`](http://www.w3.org/TR/owl-ref/#Annotations)의 인스턴스)

이 레이블은 개념의 사용의 예를 제공한다.

예

```rdf
ex:pumpkin skos:example
"baking, cooking, pumpkin seeds, pumpkin seed oil, pumpkin carving, jack o’lanterns, etc."@en.
```

**`skos:historyNote`** ([`owl:AnnotationProperty`](http://www.w3.org/TR/owl-ref/#Annotations)의 인스턴스)

이 레이블은 개념의 의미나 형식을 바꾼 의미있는 변경을 설명한다.

예

```rdf
ex:green_bean skos:historyNote
"The first "stringless" bean was bred in 1894 by Calvin Keeney while working in Le Roy, New York."@en.
```

**`skos:editorialNote`** ([`owl:AnnotationProperty`](http://www.w3.org/TR/owl-ref/#Annotations)의 인스턴스)

이 레이블은 아직 완료되지 않은 편집 작업에 대한 알림, 향후 편집이 변경될 수 있다는 알림 등 관리 지원을 위한 정보를 제공한다.

예

```rdf
ex:lima_bean skos:editorialNote "Check for alternate terms"@en.
```

**`skos:changeNote`** ([`owl:AnnotationProperty`](http://www.w3.org/TR/owl-ref/#Annotations)의 인스턴스)

이 레이블은 관리와 유지관리 목적으로 개념에 대한 세분화된 변경 사항을 문서화한다.

예

```rdf
ex:Parsnip skos:changeNote
"Moved from under 'Gourd' to under 'Root' by Priscilla Jane Smith"@en.
```

또한 개념을 문서화할 때 SKOS가 아닌 속성(예: Dublin Core의 `dct:creator`)를 사용할 수 있다는 점도 언급해야 한다.

예

```rdf
ex:spaghetti_squash dct:creator [ foaf:name "Priscilla Jane Frazier" ].
```

이 예제에서는 이 개념의 'creator'(Priscilla Jane Frazier)의 신원이 [FOAF 커뮤니티](http://wiki.foaf-project.org/w/FOAFBulletinBoard)를 통해 다른 개인과 연결되어 있음을 명시하기 위해 [Friend-of-a-Friend system(FOAF)](http://www.foaf-project.org/)을 사용하여 `dct:creator` 속성을 추가로 정의한다.

## SKOS 무결성 조건(Integrity Conditions)
SKOS Reference 문서에는 몇 가지 무결성 조건이 포함되어 있다. 무결성 조건은 주어진 데이터(예: 어휘)가 SKOS 데이터 모델과 일관성이 있는지 여부를 판단하는 데 도움이 되는 문구이다. SKOS 무결성 조건의 목적은 잘 형성되고 일관된 데이터의 구성을 장려하고 SKOS에 표현된 데이터 간의 상호 운용성을 촉진하는 것이다.

**`skos:ConceptScheme`는 `skos:Concept`과 *겹치지 않아야* 한다.**

이 조건은 SKOS 개념 체계 또는 SKOS 개념 그룹이 SKOS 개념과 같은 계층적 수준에 있지 않아야 하며 그 반대의 경우도 마찬가지이다. 예를 들어 "veggie" 어휘에서 Vegetables는 `skos:ConceptSchem`이다. 즉, Vegetables는 `skos:Concept`이어서도 안 되며, `Lima Bean`과 같은 개념 중 하나가 `skos:ConceptScheme`도 아니다.

**`skos:prefLabel`, `skos:altLabel` 및 `skos:hiddenLabel`은 쌍으로 *겹치지 않는* 속성이다.**

이 조건은 어떤 SKOS 개념도 둘 이상의 선호 레이블, 대체 레이블 및 숨겨진 레이블의 멤버가 될 수 없음을 명시한다.

**리소스에는 언어 태그당 `skos:prefLabel` 값이 오직 하나 뿐이다.**

이 조건은 어떤 SKOS 개념도 각 언어 태그에 대해 두 개 이상의 선호 레이블을 가질 수 없음을 나타낸다. 예를 들어 summer_squash라는 개념은 영어로는 "Summer Squash", 라틴어로는 "Cucurbita pepo"라는 기본 레이블을 가지며, 영어 또는 라틴어로 된 다른 선호 레이블을 갖을 수 있다.

**`skos:related`가 `skos:broaderTransitive` 속성과 *겹치지 않는*다.**

이 조건은 두 개의 SKOS 개념 관련 관계와 광의 전이 관계 모두 연결될 수 없음을 나타낸다.

**`skos:Collection`이 각각 `skos:Concept`와 `skos:ConceptScheme`과 *겹치지 않아야* 한다.**

이 조건은 SKOS 컬렉션 또는 SKOS 개념의 레이블 또는 정렬된 그룹이 SKOS 개념과 동일한 계층 수준에 있지 않아야 하며 그 반대의 경우도 마찬가지임을 나타낸다. 예를 들어 veggie 어휘에서 "Vegetables"은 `skos:Collection`이다. 즉, "Vegetables"도 `skos:Concept`이어서는 안 되며, "Lima Bean"과 같은 개념 중 하나가 `skos:Collection`이 되어서도 아니 된다.

**`skos:exactMatch`는 각 속성인 `skos:broadMatch` 및 `skos:relatedMatch`와 *겹치지 않아야* 한다.**

이 조건은 두 개의 SKOS 개념이 정확히 일치, 광의 일치 및 관련 일치 중 하나 이상에 의해 연관될 수 없음을 나타낸다.

## 문헌 검토
이 문헌 검토에서는 알려진 SKOS 변환과 검증 기술을 다룬다. 또한 과거에 구현된 수많은 SKOS 커스텀 확장과 개선 기법도 포함되어 있다. 마지막으로, SKOS를 사용한 새로운 어휘 생성 기법과 현재 이 분야의 현황에 대해 논의한다.

### SKOS 도구
정보 업계에서는 통제 어휘를 SKOS 형식으로 변환하기 위한 연구 개발 노력을 기울이고 있으며, 여러 가지 기술과 방법이 제안되고 있다. 사람이나 그룹이 통제 어휘를 수동으로 변환하는 것도 가능하지만 시간이 많이 걸리고 오류가 발생할 가능성이 높다. 어휘를 SKOS 형식으로 자동 변환하고, SKOS 형식의 어휘 서식 품질을 검증하기 위한 어플리케이션(예: [PoolParty online SKOS Consistency Checker](http://qskos.poolparty.biz/login))이 개발되었으며, 가장 최근에는 SKOS 어휘의 품질과 유효성을 개선하기 위한 어플리케이션(예: [Skosify](http://demo.seco.tkk.fi/skosify/skosify) 도구를 사용하여 RDFS와 OWL로 작성된 어휘를 SKOS 형식으로 변환 및 개선할 수 있음)이 개발되었다. 여기에서는 SKOS 변환과 유효성 검사 기술을 다룬다. 과거에 구현된 수많은 SKOS 커스텀 확장과 개선 기법도 문서화되어 있다. 마지막으로, SKOS를 사용한 어휘 생성을 위한 새로운 기법과 현재 이 분야의 현황에 대해 논의하고자 한다.

### 변환 기술(Conversion Rechniques)
2001년, Semantic Web Advanced Development for Europe (SWAD-E)는 Migrating Thesauri to the Semantic Web: Guidelines and Case Studies for Generating RDF Encodings of Existing Thesauri를 발표했다. 이는 기존 시소러스 시스템을 RDF 기반 시소러스 시스템으로 마이그레이션하기 위한 가이드라인과 방법을 제시하는 시소러스 연구 프로토타입이다. 이 문서에서 설명하는 프로세스는 세 단계로 구성되어 있다. 첫째, 시소러스에 대한 RDF 인코딩이 생성된다. 둘째, 오류 검사와 유효성 검사 프로세스를 통해 인코딩을 수행한다. 셋째, 인코딩을 웹에 게시한다. 첫 번째 단계에서는 시소러스의 기존 또는 용어 중심의 보기가 개념 중심의 보기로 변환된다. 따라서 시소러스에서 각 "선호 용어"는 "개념"에 대한 "선호 레이블"이 된다. 각 "선호 레이블"에는 `skos:prefLabel` 태그가 지정되고, 각 개념에는 `skos:concept` 태그가 지정된다. 시소러스에 있는 각 "concept"에는 웹을 통해 관련 개념의 URI에 연결할 수 있는 고유한 URI가 할당된다. 어휘의 모든 개념에 대해 고유하고 영구적인 URI를 지정하는 기술을 통해 기계는 인간이 이러한 유형의 관계를 본질적으로 이해하는 방식과 유사하게 개념 간의 관계를 이해할 수 있다.

Assem, et al.(2006)은 SWAD-E 문서의 1단계에서 사용된 기술을 확장하였다. 이 논문에서는 시소러스의 용어 중심적 관점과 개념 중심적 관점을 효과적으로 연결할 수 있는 세 가지 활동을 제안하였다. 첫째, 시소러스의 디지털 형식과 문서를 분석하여 시소러스의 특징과 인코딩 방식을 결정한다. 둘째, 시소러스 데이터 항목과 SKOS RDF 간의 매핑을 정의한다. 셋째, 변환 프로그램 또는 알고리즘을 생성한다. 저자들은 기존 URI가 존재할 경우 이를 식별하는 하위 활동을 언급하였다. URI가 존재하지 않는 경우, 시소러스의 용어 지향적 관점에서 저자는 무작위로 생성된 고유 식별자를 만들거나 고유한 선호 용어의 이름을 사용하여 URI를 생성할 것을 제안한다.

Assem, et al.은 새로운 기술을 기존의 세 가지 시소러스인 Integrated Public Sector Vocabulary (IPSV), Common Thesaurus for Audiovisual Archives (GTAA)와 Medical Subject Headings에 적용하였다. 이러한 특정 시소러스는 대중성이 높고 복잡성이 다양하기 때문에 선택되었다. 저자들은 가장 크고 복잡한 시소러스인 MeSH를 변환하는 데 어려움이 있지만, 이러한 어려움은 이 기술의 적용 가능성의 경계를 파악하는 데 도움이 된다는 사실을 발견했다. MeSH에는 여러 유형의 지식 또는 복합 개념이 결합된 텍스트 노트가 포함되어 있다는 사실로 인해 저자들은 일부 시소러스는 직접적인 SKOS 대응어가 존재하지 않는 복잡한 구조를 가지고 있다는 결론을 내렸다. 또한 IPSV와 MeSH에는 용어에 대한 관리 정보가 포함되어 있는데, 이는 SKOS 표준으로 표현할 수 없었다. 저자들은 또한 연구 당시 검증 기술의 부족으로 인해 SKOS RDF의 유효성 검사가 어렵다고 언급하였다.

2008년 Summers, at al은 논문 "LCSH, SKOS and Linked Data"에서 Library of Congress Subject Headings (LCSH)를 SKOS로 변환하는 기술을 발표했다. 이 연구에서 저자들은 MARC 서지 기록의 콘텐츠를 사용하여 LCSH 용어를 수집하고 이를 해당 SKOS 개념에 매핑하였다. 예를 들어, 모든 LCSH에  Library of Congress Control Numbers 가 부여되고, `skos:Concept`에 매핑되며, 각 개념에 대한 고유 URI를 생성하는 데 사용되었다. 사전 조정된 LCSH 용어 또는 해당 용어 조합에 대한 검색을 예상하여 이전에 함께 결합된 LCSH 용어는 하나 이상의 용어 또는 개념을 나타내므로 SKOS에서 문제를 일으킬 가능성이 있지만, 이 기술에서는 하나의 개념으로 간단히 평탄화되었다. 이러한 변환을 수행하기 위해 저자들은 Python 프로그래밍 언어를 사용하여 코드를 작성하고 오픈 소스 MARCXML 및 RDF 처리 도구를 사용하여 객체 지향 스트리밍 인터페이스를 생성하여 URI를 발행하고 서로 연결하였다. 또한, 저자들은 SKOS의 확장을 통해 LCSH 용어의 전체 의미를 SKOS 형식으로 포착할 수 있으며, Doublin Core와 같은 다른 시맨틱 웹 어휘를 통합하면 SKOS 어휘를 더욱 의미 있게 만들 수 있을 것이라고 제안하였다.

SWAD-E의 초기 변환 방법은 Neubert가 2009년 논문 "Bringing the ‘Thesaurus for Economics’ on to the Web of Linked Data"에서 채택한 것이다. Neubert는 Thesaurus for Economics (STW)가 영어와 독일어 용어로 구성되어 있다는 점을 감안할 때 SKOS에 내장된 다국어 기능이 유용하다는 사실을 발견했다. 저자는 STW를 SKOS로 변환하는 것은 간단했다고 말한다. 그러나 그가 연구할 당시에는 W3C에서 `skos:notation`을 비롯한 몇 가지 새로운 SKOS 클래스를 도입한 상태였다. Neubert는 2006년에 Assem, et al.이 SKOS에 부족하다고 지적했던 내부 경영 정보 표기법을 이제 SKOS가 수용할 수 있게 되었다는 사실을 활용할 수 있었다.

### 유효성 검사 기술 (Validation Techniques)
Neubert(2009)는 SKOS 어휘의 불일치 여부를 확인하기 위해 SPARQL 쿼리를 사용하는 방법도 논의하였다. 이 글에서는 개념 간의 비논리적 링크를 확인하는 쿼리를 통해 구현자가 이러한 불일치를 발견할 수 있도록 SKOS 어휘를 SPARQL 서버에 로드하고 불일치 검사를 실행하는 프로세스에 대해 설명한다. 또한 Neubert는 이 프로세스가 불일치 검사를 일상적으로 수행할 수 있다면 시소러스 유지보수를 개선할 수 있을 것이라고 언급했다. Neubert가 연구할 당시에는 SKOS 어휘의 자동 검증이라는 개념이 생소했다. 이 유효성 검사 프로세스의 한 가지 단점은 SPARQL 쿼리를 입력하는 구현자가 이전에 예상했던 불일치만 찾아낼 수 있다는 것이다. 쿼리되지 않은 불일치는 SPARQL 서버에서 식별되지 않는다.

2012년 논문 "Improving the Quality of SKOS Vocabularies with Skosify"에서 Suominen과 Hyvönen은 SKOS 어휘의 품질을 검사할 수 있는 도구를 제안하였다. 저자들은 기존 SKOS 어휘의 품질과 유효성이 부족하다는 점을 이 새로운 도구의 개발 이유로 들며, PoolParty Online SKOS Consistency Checker 또는 [풀파티](http://www.poolparty.biz/)라고 명명했다. 연구진은 SKOS 어휘를 검사하는 11가지 유효성 검사 기준 목록을 만들었다. 이 기준은 W3C SKOS Reference 문서와 무료로 제공되는 14개의 SKOS 어휘에 대한 저자들의 검토를 통해 수집되었다. 저자들은 모든 중대형 어휘가 적어도 한 번 이상의 유효성 검사에서 불합격했으며, 이는 SKOS 무결성 제약 조건 중 일부를 충족하지 못한다는 것을 의미하는 것이다. 14개의 어휘 중 11개의 유효성 검사 기준을 모두 통과한 어휘는 단 하나뿐이었다.

### 커스텀 확장 (Custom Expansions)
Assem, et al.의 연구와 Neubert의 연구에서 볼 수 있듯이, 통제 어휘는 종종 SKOS에 직접 대응하는 어휘가 없는 구성을 포함한다. [SKOS Primer]()에 명시된 바와 같이, SKOS는 특정 어휘를 전문화하기 위해 언어 구성을 간단하게 확장할 수 있도록 설계되었다. 이는 확장 `rdfs:subClassOf`를 사용하여 SKOS 구문을 설명함으로써 가능하다. Neubert는 STW 시소러스와 함께 사용하기 위해 두 가지 SKOS 확장을 제안했다. 첫째, 그는 `skos:Concept`를 두 개의 "하위 클래스"로 분할한다. 표준 용어 또는 설명자와 함께 STW에는 사용자 정보 검색을 지원하는 데 사용되는 약 500개의 클래스로 구성된 분류 체계가 포함되어 있기 때문에 이러한 변경이 선택되었다. Neuvert는 `skos:Concept`를 두 부분으로 나눈다. 개념에 대한 `zbwext:Descriptor rdfs:subClassOf skos:Concept`와 클래스에 대한 `zbwext:Thsys rdfs:subClassOf skos:Concept`이다. 이 접근 방식은 개념 간의 광범위하고 좁은 관계 측면에서 잘 정의된 의미를 허용하고 사용자가 개념과 클래스를 모두 검색할 수 있도록 한다. 또한, Neubert는 특정 상황에서 다른 선호 레이블 대신 지정된 선호 레이블로 사용자를 안내하는 노트에 대해 `zbwext:useInsteadNote rdfs:subClassOf skos:note`라는 표기법을 사용해 새로 도입된 `skos:note`를 확장했다.

### 개선 기술 (Improvement Techniques)
Suominen과 Hyvönen(2012)은 유효성 검사 도구 PoolParty와 함께 SKOS 어휘의 품질과 유효성을 향상시키기 위해 개발된 도구인 [Skosify](http://www.seco.tkk.fi/tools/skosify/)를 소개하였다. Skosify는 하나 이상의 SKOS 파일을 읽고 오류와 문제가 확인되었을 뿐만 아니라 수정된 파일을 출력할 수 있는 명령어 도구(command line tool)이다. Skosify는 논문에서 앞서 언급한 11개의 유효성 검사 기준 중 9개를 해결할 수 있다. 14개의 어휘에 대해 테스트를 거친 후, Skosify는 이 9개의 범주에서 모두 문제를 수정하였다. 이 도구는 기본 언어가 제공되는 경우 누락된 언어 태그를 수정하고, 레이블이 지정되지 않은 개념 체계를 탐지하여 지정된 레이블을 추가하며, 속성 값을 둘러싼 불필요한 공백을 제거할 수 있다. Skosify는 어휘의 개념 간의 관계와 관련된 보다 정교한 문제를 인식하고 수정할 수 있다. 이 도구는 최상위 개념을 식별하고 해당 개념 체계에 **`hasTopConcept`**와 **`topConceptOf`** 관계를 추가할 수 있으며, 단일 개념에 대해 하나 이상의 preLabel을 지정한 것을 인식하고 오류를 수정할 수 있으며, 잘못된 레이블이 지정된 개념을 식별할 뿐만 아니라 컬렉션으로 식별한다. 또한 이 도구는 두 개의 서로 다른 레이블 속성을 사용하여 개념이 레이블과 연결된 경우 인식하고 덜 중요한 속성을 제거하며, 개념이 서로 연결되지 않은 방식으로 연결된 경우 인식하고 광범위한 계층 구조를 비활성화하지 않고 관련된 관계 선언을 제거하며, 자신과 더 넓은 관계를 갖는 순환 또는 개념을 인식하고 불쾌감을 주는 관계를 제거한다.

### SKOS로 어휘 생성 (Vocuburary Creation with SKOS)
2010년, Gerbé와 Kerhervé는 그들의 논문 "A Model-Driven Approach to SKOS Implementation"을 통해 어휘 생성에 대한 새로운 접근법을 제안했다. 그들의 기술은 SKOS 개념 모델을 구조화된 통제 어휘에 대한 메타 모델로 보는 것을 포함한다. 모델 관리와 모델 엔지니어링 접근법을 사용하여 저자들은 유연하고 확장 가능한 어휘가 관리되고 생성될 수 있으며, 이는 비전통적이거나 매우 복잡한 어휘에 유용할 수 있다고 기술했다. 모델 연산자, 또는 맵, 매치, 병합 및 구성과 같은 모델 관리에 사용되는 프롬프트는 용어 지향 어휘를 개념 지향 플랫폼에 매핑하는 데 사용될 수 있다. Gerbé와 Kerhervé는 SKOS 어휘 개발에 사용하기 위해 메타 모델과 SKOS 엔진을 소개했다. 메타 모델은 데이터베이스 스키마와 거의 동일한 방식으로 표현되며 MySQL 데이터베이스 시스템에 의해 지원된다. SKOS 엔진은 어휘 내용을 채우고 시각화하는 데 사용하기 위한 인터페이스를 갖춘 데이터베이스로 구축된다. 저자들은 도구가 단어를 가져오고 내보내고 병합할 수도 있다고 한다. SKOS 어휘 생성에 대한 모델 중심 데이터베이스 접근법은 고급 데이터 저장 시스템과 그래픽 사용자 인터페이스를 제공함으로써 SKOS 어휘와 함께 작업하기 위한 도구 기능의 정교함을 새로운 수준으로 끌어올린다.

### 최신 기술 (State of Art)
2012년 Manaf, Bechhofer와 Stevens는 웹상에서 SKOS 어휘의 현황에 대한 설문조사를 발표하였다. 총 478개의 어휘가 SKOS 어휘의 주어진 정의를 준수하는 것으로 확인되었다. 해당 어휘에 대한 분석으로는 SKOS 구문의 사용 조사, SKOS 의미 관계 및 어휘 레이블의 사용, 어휘의 계층적 및 연상 관계, 분기 요인 및 깊이 측면의 구조 등이 있다.

연구원들은 각 SKOS 어휘에 대한 다음의 데이터를 수집하였다. 그것은 SKOS 개념의 수; 각 SKOS 개념의 깊이와 개념 계층의 깊이; SKOS를 위한 링크의 수: 더 넓어진, SKOS:더 좁은, 그리고 SKOS:관련 속성; 다른 개념들과 연결되지 않은 총 개념의 수; SKOS:더 좁은 관계이지만 SKOS:더 넓은 관계가 없는 총 개념의 수; 그리고 최대 SKOS:더 넓은 속성의 수 등이다. 연구원들에 따르면, SKOS 개념과 개념 표기는 SKOS 어휘의 핵심이지만, 연구에 있는 모든 SKOS 어휘들이 개념에 SKOS 어휘 표기를 사용하는 것은 아니다. 연구된 SKOS 어휘의 약 1/3이 어떤 SKOS 의미 관계도 사용하지 않고, 용어 목록의 범주에 속한다. 연구원들은 모든 출판된 SKOS 어휘가 어휘에 존재하는 SKOS 개념을 명시적으로 선언하지 않는다는 것을 발견했다. 조사 결과는 특히 이러한 어휘를 사용하는 어플리케이션의 생성을 고려할 때, 웹에 출판된 SKOS 어휘의 모델링 스타일에 대한 더 나은 이해를 제공하는 데 도움이 될 수 있다.


<a name="footnote_1">1</a> 이 페이지는 [SKOS: A Guide for Information Professionals](https://www.ala.org/alcts/resources/z687/skos)을 편역한 것임.
